package gui.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import application.model.Brand;
import application.model.EnergyMark;
import application.model.Product;
import application.model.ProductType;
import application.model.RecieptLine;
import application.model.TVResolution;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import storage.Storage;

public class StockControlController {

	private static Pane pane;
	private static Stage stage;
	private Product foundProduct;
	
	
		@FXML
	    private TextField txfEAN,txfCreateEAN,txfAmount,txfpurchaseNetPrice,txfTVSize,txfTVHDMI,txfTVUSB,txfSmartScreen,txfSmartCPU,txfSmartGB,txfSmartCameraMP,txfSmartInternalMemory,txfSmartChargeType,txfSmartWeight;

	    @FXML
	    private Button btnFindProduct,btnSubmit,btnLoadImage,btnCreateProduct;

	    @FXML
	    private CheckBox chReserved,chbWifi,chbSmartTv,chbBluetooth;

	    @FXML
	    private Label lblProductInformation,lblFoundProduct,lblImage;

	    @FXML
	    private ImageView imgProduct;

	    @FXML
	    private HBox hbProductInformation,hbTV,hbPhone;

	    @FXML
	    private ComboBox<String> cbTVTeknology;
	    
	    @FXML
	    private ComboBox<TVResolution> cbTVResolution;
	    
	    @FXML
	    private ComboBox<ProductType> cbType;

	    @FXML
	    private TextField txfModel;

	    @FXML
	    private TextField txfPrice;

	    @FXML
	    private ComboBox<Brand> cbBrand;

	    @FXML
	    private ComboBox<EnergyMark> cbEnergyMark;
	    
	    private File image;
	    
	    public static void initWindow() {
			 try {

					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(MainApp.class.getResource("view/StockControlView.fxml"));
					pane = (Pane) loader.load();
					stage = new Stage();
					stage.setTitle("Lagerstyring");
					stage.setScene(new Scene(pane));
					stage.show();			
				}
				catch (IOException e) {
					System.out.println(e.getMessage());
				}
		}
	    
	    @FXML 
		void initialize() {
	    	
	    	cbTVTeknology.getItems().add("LCD");
	    	cbTVTeknology.getItems().add("LED");
	    	cbTVTeknology.getItems().add("OLED");
			cbTVTeknology.getItems().add("Plasma");
			cbTVTeknology.getItems().add("Projektor");

			cbTVResolution.getItems().add(TVResolution.HD_Ready);
			cbTVResolution.getItems().add(TVResolution.Full_HD);
			cbTVResolution.getItems().add(TVResolution.Ultra_HD);
				
			cbBrand.getItems().addAll(Storage.getAllBrands());

			cbEnergyMark.setPromptText("Vælg energimærke");
			cbEnergyMark.getItems().addAll(Storage.getAllEnergymarks());
	    	
			cbType.getItems().add(ProductType.TV);
			cbType.getItems().add(ProductType.Smartphone);
			
	    	// Listener
			cbType.valueProperty().addListener(event -> getProductFields());
	    	
	    	// Button action
			btnFindProduct.setOnAction(event -> searchProductByEAN());
			btnSubmit.setOnAction(event -> submitRegistration());
			btnLoadImage.setOnAction(event -> chooseImage());
			btnCreateProduct.setOnAction(event -> createProduct());
	    }
	    
	    public void searchProductByEAN() {

			lblFoundProduct.setText("");

			try {
				foundProduct = Service.findProductByEAN(Integer.parseInt(txfEAN.getText()));
				lblFoundProduct.setText(foundProduct.getBrand().getName() + " " + foundProduct.getModel());
				txfpurchaseNetPrice.setText("" + foundProduct.getPurchaseNetPrice());
				showProductInformation(foundProduct);
			} catch (Exception e) {
				lblFoundProduct.setText("Der blev desværre intet produkt fundet med dette EAN-nummer");
			}
				
		}

		public void showProductInformation(Product foundProduct) {

			// Image
			lblProductInformation.setText(foundProduct.desciptionToString());
			try {
				imgProduct.setImage(Service.findProductImage(foundProduct));
			} catch (FileNotFoundException e) {
				imgProduct.setImage(
						new Image("file:" + System.getProperty("user.dir") + "/src/images/productImages/no_image.png"));
			}
		}

		public void submitRegistration() {
			if (foundProduct != null) {
				Service.addProductToStock(foundProduct, Double.parseDouble(txfpurchaseNetPrice.getText()),
						Integer.parseInt(txfAmount.getText()));

				txfAmount.setText("");
				txfEAN.setText("");
				txfpurchaseNetPrice.setText("");
				lblFoundProduct.setText("");
				this.foundProduct = null;
			}
		}
		
		
		public void createProduct() {

			if (cbBrand.getSelectionModel().getSelectedItem() != null
					&& cbEnergyMark.getSelectionModel().getSelectedItem() != null) {

				if (cbType.getSelectionModel().getSelectedItem().equals(ProductType.TV)) {

					Service.createTv(txfModel.getText(), Long.parseLong(txfCreateEAN.getText()),
							cbBrand.getSelectionModel().getSelectedItem(), false,
							Double.parseDouble(txfPrice.getText()), 0, ProductType.TV, Integer.parseInt(txfTVSize.getText()),
							cbTVTeknology.getSelectionModel().getSelectedItem(), cbTVResolution.getSelectionModel().getSelectedItem(),
							Integer.parseInt(txfTVHDMI.getText()), Integer.parseInt(txfTVUSB.getText()),
							chbWifi.isSelected(),chbBluetooth.isSelected(),chbSmartTv.isSelected());
				} else if (cbType.getSelectionModel().getSelectedItem().equals(ProductType.Smartphone)) {

					Service.createSmartphone(txfModel.getText(), Long.parseLong(txfCreateEAN.getText()),
							cbBrand.getSelectionModel().getSelectedItem(), false,
							Double.parseDouble(txfPrice.getText()), 0,
							null, Double.parseDouble(txfSmartScreen.getText()), txfSmartCPU.getText(),
							Integer.parseInt(txfSmartGB.getText()), Integer.parseInt(txfSmartInternalMemory.getText()),
							Double.parseDouble(txfSmartWeight.getText()), Double.parseDouble(txfSmartCameraMP.getText()),
							txfSmartChargeType.getText());
				} else {
					Service.createProduct(txfModel.getText(), Long.parseLong(txfCreateEAN.getText()),
							cbBrand.getSelectionModel().getSelectedItem(), false,
							Double.parseDouble(txfPrice.getText()), 0, null);
				}

				if (image != null) {

					String filename = cbBrand.getSelectionModel().getSelectedItem().getName().toLowerCase()
							+ txfModel.getText().toLowerCase() + ".png";

					Service.saveProductImage(image, filename);
				}
			}

		}
		
		public void chooseImage() {

			FileChooser fcImage = new FileChooser();

			fcImage.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png"));

			fcImage.setTitle("Produkt billede");

			image = fcImage.showOpenDialog(stage.getOwner());

			if (image != null) {
				lblImage.setText(image.getName());
			}

		}

		public void getProductFields() {

			if (cbType.getSelectionModel().getSelectedItem().equals(ProductType.TV)) {
				hbTV.setVisible(true);
				hbPhone.setVisible(false);
			} else if (cbType.getSelectionModel().getSelectedItem().equals(ProductType.Smartphone)) {
				hbTV.setVisible(false);
				hbPhone.setVisible(true);
			} else {
				hbTV.setVisible(false);
				hbPhone.setVisible(false);
			}

		}
}
