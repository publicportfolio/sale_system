package gui.view;

import java.io.IOException;

import application.model.Price;
import application.model.Product;
import application.model.Reciept;
import application.model.RecieptLine;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class AddProductToRecieptController {

	private static Pane pane;
	private static Stage stage;
	private static Reciept reciept;
	private static RecieptLine recieptLine;
	private static Product product;
	private static SaleReturnController parent;
	
	@FXML
    private Label lblProduct,lblAvance;

    @FXML
    private TextField txfPrice;

    @FXML
    private ComboBox<Integer> cbAmount;

    @FXML
    private CheckBox cbInsurance;

    @FXML
    private Button btnAddProduct;

	
	 public static void initWindow(RecieptLine inputRecieptLine) {
		 try {
			 	recieptLine = inputRecieptLine;
			 
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/AddProductToRecieptView.fxml"));
				pane = (Pane) loader.load();
				stage = new Stage();
				stage.setTitle("Opdater ordrelinje");
				stage.setScene(new Scene(pane));
				stage.show();

				
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	 
	 public static void initWindow(SaleReturnController inputParent,Product inputProduct, Reciept inputReciept) {
		 try {
				reciept = inputReciept;
				product = inputProduct;
				parent = inputParent;
				
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/AddProductToRecieptView.fxml"));
				pane = (Pane) loader.load();
				stage = new Stage();
				stage.setTitle("Tilføj");
				stage.setScene(new Scene(pane));
				stage.show();
				
			
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	 
	 
	@FXML 
	void initialize() {
		
		if (recieptLine != null && recieptLine.getPrice().getInsurance() != null) {
			cbInsurance.setSelected(true);
		}
	
		if (recieptLine != null) {

			if (recieptLine.getProduct().getStockAmount() == 0) {
				for (int i = 1; i <= this.recieptLine.getAmount(); i++) {
					cbAmount.getItems().add(i);
				}
			} else {
				for (int i = 0; i <= this.recieptLine.getProduct().getStockAmount()
						+ this.recieptLine.getAmount(); i++) {
					cbAmount.getItems().add(i);
				}

			}
			cbAmount.getSelectionModel().select(recieptLine.getAmount());
		} else {
			for (int i = 1; i <= this.product.getStockAmount(); i++) {
				cbAmount.getItems().add(i);
			}
			cbAmount.getSelectionModel().select(0);
		}
	
		if (recieptLine != null) {
			txfPrice.setText("" + recieptLine.getPrice());
		} else {
			txfPrice.setText("" + product.getSuggestedRetailPrice());
		}
		
		btnAddProduct.setText((recieptLine != null ? "Opdater" : "Tilføj"));
	
		if (recieptLine != null) {
			System.out.println(recieptLine.getProductModel());
			lblProduct.setText(recieptLine.getProduct().getBrand() + " " + recieptLine.getProduct().getModel());
		} else {
			lblProduct.setText(product.getBrand() + " " + product.getModel());
		}
	
		getAvance();
		findInsurance();

		// Listener
		cbAmount.getSelectionModel().selectedItemProperty().addListener(event -> updatePrice());

		txfPrice.textProperty().addListener(event -> getAvance());
		txfPrice.textProperty().addListener(event -> findInsurance());

		// Button actions
		btnAddProduct.setOnAction(event -> addProduct());
		
	}
	
	public void addProduct() {

		Price finalPrice = new Price(Double.parseDouble(txfPrice.getText()));

		if (cbInsurance.isSelected()) {
			Service.addInsuranceToPrice(cbAmount.getSelectionModel().getSelectedItem(), finalPrice,
					(recieptLine != null ? recieptLine.getProduct() : product));
		}
		if (recieptLine != null) {
			if (cbAmount.getSelectionModel().getSelectedItem() == 0) {
				Service.removeRecieptLine(reciept, recieptLine);
				
			} else {
				Service.updateRecieptLine(recieptLine, finalPrice, cbAmount.getSelectionModel().getSelectedItem());
			}

		} else {
			Service.createRecieptLine(reciept, product, finalPrice, cbAmount.getSelectionModel().getSelectedItem());

		}
		((SaleReturnController)parent).updateInformation();
	
		this.recieptLine = null;
		stage.close();
	}

	public void getAvance() {
		if (!txfPrice.getText().equals("")) {
			lblAvance.setText(String.format("%.2f",
					Service.getAvanceOnProduct((recieptLine != null ? recieptLine.getProduct() : product),
							Double.parseDouble(txfPrice.getText()), cbAmount.getSelectionModel().getSelectedItem()))
					+ "%");
		}
	}

	public void updatePrice() {
		txfPrice.setText(String.format("%.2f",
				(recieptLine != null ? recieptLine.getProduct() : product).getSuggestedRetailPrice()
						* cbAmount.getSelectionModel().getSelectedItem()));
	}

	public void findInsurance() {
		if (!txfPrice.getText().equals("")) {

			Price tempPrice = new Price(Double.parseDouble(txfPrice.getText()));

			double insurancePrice = Service.findInsurancePrice(tempPrice,
					cbAmount.getSelectionModel().getSelectedItem(),
					(recieptLine != null ? recieptLine.getProduct() : product));

			cbInsurance.setText(insurancePrice + "");
		}
	}

	
	
}
