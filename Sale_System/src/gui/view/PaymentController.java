package gui.view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;

import application.model.Payment;
import application.model.PaymentMethod;
import application.model.Reciept;
import application.service.Service;
import gui.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PaymentController {

	private static Pane pane;
	private static Stage stage;
	
	@FXML
	private VBox hbPaymentMethods;
	
	@FXML
	private TableView<Payment> tblPayments;

	@FXML
	private TableColumn<Payment, LocalDate> columnPaymentDate;

	@FXML
	private TableColumn<Payment, Double> columnPaymentAmount;

	@FXML
	private TableColumn<Payment, PaymentMethod> columnPaymentType;
	
    @FXML
    private TextField txfAmount;

    @FXML
    private Button btnAdd,btnDelete,btnEdit;
	
	private RadioButton rbMobilePay;
	private RadioButton rbCreditCard;
	private RadioButton rbCash;
	private RadioButton rbGiftCard;
	private ToggleGroup tglPayment;
	private static Reciept actualReciept; 
	
	public static void initWindow(Reciept reciept) {
		
		actualReciept = reciept;
		
		 try {			 
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/PaymentView.fxml"));
				pane = loader.load();
				stage = new Stage();
				stage.setTitle("Betaling");
				stage.setScene(new Scene(pane));
				stage.show();		
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	
	@FXML
	void initialize() {		
	
    	// table columns
    	columnPaymentDate.setCellValueFactory(new PropertyValueFactory<>("date"));
    	columnPaymentAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
    	columnPaymentType.setCellValueFactory(new PropertyValueFactory<>("method"));		
		
		tblPayments.getItems().setAll(Service.getPaymentsForReciept(actualReciept));	
		
		tglPayment = new ToggleGroup();
		
		rbMobilePay = new RadioButton("Mobilepay");
		rbMobilePay.setUserData(PaymentMethod.MobilePay);
		rbMobilePay.setSelected(true);
		
		rbCreditCard = new RadioButton("Kredit kort");
		rbCreditCard.setUserData(PaymentMethod.Kort);
		
		rbCash = new RadioButton("Kontant");
		rbCash.setUserData(PaymentMethod.Kontant);

		rbGiftCard = new RadioButton("Gavekort");
		rbGiftCard.setUserData(PaymentMethod.Gavekort);
		
		// Image
		try {
			rbMobilePay.setGraphic(new ImageView(new Image("file:" +System.getProperty("user.dir") + "/src/images/paymentIcons/mobilepay.png")));
			rbCreditCard.setGraphic(new ImageView(new Image("file:" +System.getProperty("user.dir") + "/src/images/paymentIcons/creditcard.png")));
			rbCash.setGraphic(new ImageView(new Image("file:" +System.getProperty("user.dir") + "/src/images/paymentIcons/cash.png")));
			rbGiftCard.setGraphic(new ImageView(new Image("file:" +System.getProperty("user.dir") + "/src/images/paymentIcons/giftcard.png")));
		} catch (Exception e) {
		System.out.println(e.getMessage());
		}
		
		tglPayment.getToggles().add(rbMobilePay);
		tglPayment.getToggles().add(rbCash);
		tglPayment.getToggles().add(rbCreditCard);
		tglPayment.getToggles().add(rbGiftCard);
		
		hbPaymentMethods.getChildren().add(rbMobilePay);
		hbPaymentMethods.getChildren().add(rbCash);
		hbPaymentMethods.getChildren().add(rbCreditCard);
		hbPaymentMethods.getChildren().add(rbGiftCard);
		
		if (Service.getBalanceOnReciept(actualReciept) <= 0) {
			btnAdd.setDisable(true);
			txfAmount.setEditable(false);
		}
		
		//Balance 
		txfAmount.setText(String.format("%.2f",Service.getBalanceOnReciept(actualReciept)));
		
		//Actions
		btnAdd.setOnAction(event -> addPaymentToReciept());
		btnEdit.setOnAction(event -> editPayment());
		btnDelete.setOnAction(event -> deletePayment());
		
		//Listener
		tblPayments.getSelectionModel().selectedItemProperty().addListener(event -> unlockDeleteEdit());
		
	}
	
	private void addPaymentToReciept() {
		if (btnAdd.getText().equals("Gem")) {
			Service.updatePayment(tblPayments.getSelectionModel().getSelectedItem(), (PaymentMethod)tglPayment.getSelectedToggle().getUserData(),Double.parseDouble(txfAmount.getText()));
			btnAdd.setText("Tilføj");
		}
		else {
			Service.addPaymentToReciept(actualReciept, Double.parseDouble(txfAmount.getText()), (PaymentMethod)tglPayment.getSelectedToggle().getUserData());
		}
		
		updateInformation();
	}
	
	private void unlockDeleteEdit() {	
		if (tblPayments.getSelectionModel().getSelectedItem() != null) {
			btnDelete.setDisable(false);
			btnEdit.setDisable(false);
		}
		else {
			btnDelete.setDisable(true);
			btnEdit.setDisable(true);
		}
	}
	private void editPayment() {	
		if (tblPayments.getSelectionModel().getSelectedItem() != null) {
			txfAmount.setText(String.format("%.2f", tblPayments.getSelectionModel().getSelectedItem().getAmount()));
			btnAdd.setDisable(false);
			txfAmount.setEditable(true);
			btnAdd.setText("Gem");
		}
	}
	private void deletePayment() {	
		
		if (tblPayments.getSelectionModel().getSelectedItem() != null) {
			Service.removePayment(tblPayments.getSelectionModel().getSelectedItem(), actualReciept);
		}
		updateInformation();
	}
	
	private void updateInformation() {
		txfAmount.setText(String.format("%.2f",Service.getBalanceOnReciept(actualReciept)));
		tblPayments.getItems().clear();
		tblPayments.getItems().setAll(Service.getPaymentsForReciept(actualReciept));	
		if (Service.getBalanceOnReciept(actualReciept) <= 0) {
			btnAdd.setDisable(true);
			txfAmount.setEditable(false);
		}
		else {
			btnAdd.setDisable(false);
			txfAmount.setEditable(true);
		}
	}
}
