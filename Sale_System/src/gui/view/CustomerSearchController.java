package gui.view;

import java.io.IOException;

import application.model.Customer;
import application.model.RecieptLine;
import gui.MainApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import storage.Storage;

public class CustomerSearchController {

	private static Pane pane;
	private static Stage stage;
	private static SaleReturnController parent;
	private static ObservableList<Customer> customers = FXCollections.observableArrayList();
	private static Customer actuelCustomer;
	
	@FXML
    private TableView<Customer> tblCustomers;

    @FXML
    private TableColumn<Customer, Integer> columnNumber;

    @FXML
    private TableColumn<Customer, String> columnName;

    @FXML
    private TableColumn<Customer, String> columnAddress;

    @FXML
    private TableColumn<Customer, Integer> columnPhone;

    @FXML
    private TableColumn<Customer, String> columnEmail;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
	
	 public static void initWindow(ObservableList<Customer> customerList,SaleReturnController inputParent) {
		 try {
			 actuelCustomer = null;
			 customers = customerList;
			 parent = inputParent;
			 
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/CustomerSearchView.fxml"));
				pane = (Pane) loader.load();
				stage = new Stage();
				stage.setTitle("Søg efter kunde");
				stage.setScene(new Scene(pane));
				stage.show();

				
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	 
	 @FXML void initialize() {
		 
		 columnNumber.setCellValueFactory(new PropertyValueFactory<>("customerNumber"));
		 columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
		 columnAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
		 columnPhone.setCellValueFactory(new PropertyValueFactory<>("phone"));
		 columnEmail.setCellValueFactory(new PropertyValueFactory<>("email"));

		 tblCustomers.getItems().addAll(customers);

		// Button action
		btnOk.setOnAction(event -> addCustomerToReciept());
		btnCancel.setOnAction(event -> closeWindow());
	 }
	 
	 @FXML
	 public void addCustomerToReciept()
	 {
		 Customer marked =  (Customer) tblCustomers.getSelectionModel().getSelectedItem();
		 
		 if (marked != null) {
			 parent.addCustomerToReciept(marked);
			 parent.getCustomerInfo();
			 stage.close();
		}		 		 
	 }
	 @FXML
	 public void closeWindow() {
		 stage.close();
	 }
	 
	 
	 
}
