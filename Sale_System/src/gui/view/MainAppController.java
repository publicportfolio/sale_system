package gui.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import application.model.Product;
import application.model.User;
import application.service.Service;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import storage.Storage;

public class MainAppController {


    @FXML
    private Button btnSale;

    @FXML
    private Button btnStock;

    @FXML
    private Button btnCustomer;

    @FXML
    private Button btnRapport;

    
    @FXML void initialize() {
    	Service.initStorage();
    }
    
    public void openSaleWindow() {

		SaleReturnController.initWindow();
		
	}

	public void openHistoryWindow() {

		HistoryController.initWindow();

	} 

//	public void openCustomerBookWindow() {
//
//		CustomerBookController.initWindow();
//	}
//
	public void openInventoryControlWindow() {

		StockControlController.initWindow();
	}
    
}
