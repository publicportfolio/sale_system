package gui.view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import javax.swing.plaf.ActionMapUIResource;

import application.model.Customer;
import application.model.Employee;
import application.model.Product;
import application.model.Reciept;
import application.model.RecieptLine;
import application.service.Service;
import application.service.Validator;
import gui.MainApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import storage.Storage;

public class SaleReturnController {
	
	private static Pane pane;
	private static Stage stage;
	private Customer actuelCustomer = null;
	private Employee employeeLoggedIn;
	private Reciept reciept;
	private Product tempProduct = null;
	private Reciept actualReciept;
	private ObservableList<RecieptLine> recieptProducts = FXCollections.emptyObservableList();

	
	 	@FXML
	    private TextField txfProductSearch,txfEAN,txfName,txfAddress,txfZipcode,txfPhone,txfEmail,txfCustomerNumber,txfSearchCustomer,txfRecieptNumber,txfRefund;

	    @FXML
	    private Button btnProductSearch,btnNewCustomer,btnSearchCustomer,btnPayment,btnAdd,btnSearch,btnReturn,btnPrint,btnSaveOrder;

	    @FXML
	    private TableView<RecieptLine> tblRecieptProducts,tvRecieptLines;

	    @FXML
	    private TableColumn<RecieptLine, String> columnBrand,columnModel,columnReturnBrand,columnReturnModel;

	    @FXML
	    private TableColumn<RecieptLine, Integer> columnAmount,columnReturnAmount;

	    @FXML
	    private TableColumn<RecieptLine, Double> columnPrice,columnInsurance,columnReturnPrice,columnReturnInsurance;

	    @FXML
	    private Label lblemployeeLoggedIn,lblAvance,lblMessage,lblDescription,lblTotal,lblTax,lblBeforeTax;

	    @FXML
	    private ImageView imgProduct;

	     
	    
	    public static void initWindow() {
	    	
	    	try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/SaleReturnView.fxml"));
				pane = (Pane) loader.load();
				stage = new Stage();
				stage.setTitle("Salg/Retur");
				stage.setScene(new Scene(pane));
				stage.show();
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	    }
	    
	    @FXML
		 void initialize() {

	    	
	    	// Brand column
	    	columnBrand.setCellValueFactory(new PropertyValueFactory<>("ProductBrand"));
	    	columnReturnBrand.setCellValueFactory(new PropertyValueFactory<>("ProductBrand"));
	    	
	    	// Model column
	    	columnModel.setCellValueFactory(new PropertyValueFactory<>("ProductModel"));
	    	columnReturnModel.setCellValueFactory(new PropertyValueFactory<>("ProductModel"));
	    	
			// Amount column
	    	columnAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
	    	columnReturnAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));

			// Price column
	    	columnPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
	    	columnReturnPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
	    	
			// Insurance column
	    	columnInsurance.setCellValueFactory(new PropertyValueFactory<>("ProductInsurance"));
	    	columnReturnInsurance.setCellValueFactory(new PropertyValueFactory<>("ProductInsurance"));			
			
	    	// TEST USER
			employeeLoggedIn = Storage.getAllEmployees().get(0);
			lblemployeeLoggedIn.setText(employeeLoggedIn.getName() + " Nr: " + employeeLoggedIn.getSalesNumber());
			
			
			// Creating the reciept template
			LocalDate date = LocalDate.now();

			reciept = Service.createReciept(date, employeeLoggedIn);
			recieptProducts = Service.getRecieptLinesObserve(reciept);
			
			// Listener
			tblRecieptProducts.getSelectionModel().selectedItemProperty().addListener(event -> editRecieptLine());
			tvRecieptLines.getSelectionModel().selectedItemProperty().addListener(event -> selectRecieptLine());
			txfCustomerNumber.textProperty().addListener(event -> checkCustomerNumber());
			
			// Button actions
			txfEAN.setOnKeyReleased(event -> searchProduct(event.getCode()));
			btnAdd.setOnAction(event -> addToReciept(tempProduct));
			btnPayment.setOnAction(event -> addPayment());
			btnSearchCustomer.setOnAction(event -> searchCustomer());
			btnNewCustomer.setOnAction(event -> createNewCustomer());
			btnProductSearch.setOnAction(event -> searchProduct(txfProductSearch.getText()));
			btnSearch.setOnAction(event -> findReciept());
			btnReturn.setOnAction(event -> deleteRecieptLine());
			btnPrint.setOnAction(event -> printReciept());
			btnSaveOrder.setOnAction(event -> saveOrder());
	    }
	    
	    //-------------------SALE PANE---------------------------//
	    /**
		 * @param ean
		 *            : the ean on the product that you want to find
		 * @return returns the product if it exists
		 */
		public Product searchProduct(KeyCode code) {

			if (code == KeyCode.ENTER) {
				Product product = null;
				product = Service.findProductByEAN(Long.parseLong(txfEAN.getText()));
				showProductInformation(product);
				return product;
			}
			return null;
		}
		
		public void searchProduct(String searchWord) {
			ProductSearchController.initWindow(Service.findProductByText(searchWord),this);
		}

		public void searchCustomer() {
			CustomerSearchController.initWindow(Service.searchCustomer(txfSearchCustomer.getText()),this);
		}

		public void editRecieptLine() {

			if (tblRecieptProducts.getSelectionModel().getSelectedItem() != null) {
				RecieptLine rcLine = tblRecieptProducts.getSelectionModel().getSelectedItem();
				AddProductToRecieptController.initWindow(rcLine);
			}
		}

		public void addToReciept(Product product) {
			AddProductToRecieptController.initWindow(this,product, reciept);		
			updateInformation();
		}

		public void onlockFields() {
			txfName.setText("");
			txfName.setEditable(true);
			txfAddress.setText("");
			txfAddress.setEditable(true);
			txfZipcode.setText("");
			txfZipcode.setEditable(true);
			txfPhone.setText("");
			txfPhone.setEditable(true);
			txfEmail.setText("");
			txfEmail.setEditable(true);
			txfCustomerNumber.setText("");
			
		}

		public void getCustomerInfo() {
			txfSearchCustomer.setText("");
			txfName.setText(actuelCustomer.getName());
			txfName.setEditable(false);
			txfAddress.setText(actuelCustomer.getAddress());
			txfAddress.setEditable(false);
			txfZipcode.setText("" + actuelCustomer.getZipcode());
			txfZipcode.setEditable(false);
			txfPhone.setText("" + actuelCustomer.getPhone());
			txfPhone.setEditable(false);
			txfEmail.setText(actuelCustomer.getEmail());
			txfEmail.setEditable(false);
			txfCustomerNumber.setText("" + actuelCustomer.getCustomerNumber());
		}

		public void createNewCustomer() {
			onlockFields();
			txfCustomerNumber.setText("NY KUNDE");
			
		}
		
		public void clearAllFields() {
			actuelCustomer = null;
			reciept = null;
			tblRecieptProducts.getItems().clear();
			lblAvance.setText("");
			lblMessage.setText("Faktura oprettet");
			lblTotal.setText("");
			lblTax.setText("");
			lblBeforeTax.setText("");
			onlockFields();
			clearAllProductInfo();
		}

		public void saveOrder() {
			Customer customer = null;
			boolean allValid = true;
			
			Border errorBorder = new Border(new BorderStroke(Color.RED,BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));
			Border successBorder = new Border(new BorderStroke(Color.GREEN,BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));
			
			Validator bValidate = new Validator();
		
			
			if (!bValidate.isValidString(txfName.getText())) {
				txfName.setBorder(errorBorder);
				allValid = false;
			}
			else {
				txfName.setBorder(successBorder);
			}
			if (!bValidate.isValidString(txfAddress.getText())) {
				txfAddress.setBorder(errorBorder);
				allValid = false;
			}
			else {
				txfAddress.setBorder(successBorder);
			}
			if (!bValidate.isValidZipcode(txfZipcode.getText())) {
				txfZipcode.setBorder(errorBorder);
				allValid = false;
			}
			else {
				txfZipcode.setBorder(successBorder);
			}
			if (!bValidate.isValidPhone(txfPhone.getText())) {
				txfPhone.setBorder(errorBorder);
				allValid = false;
			}
			else {
				txfPhone.setBorder(successBorder);
			}
			if (!bValidate.isValidEmail(txfEmail.getText())) {
				txfEmail.setBorder(errorBorder);
				allValid = false;
			}
			else {
				txfEmail.setBorder(successBorder);
			}
			
			if (allValid) {
				if (actuelCustomer == null) {		
					customer = new Customer(txfName.getText(), txfAddress.getText(), Integer.parseInt(txfZipcode.getText()),
							Integer.parseInt(txfPhone.getText()), txfEmail.getText());
				} else {
					customer = actuelCustomer;
				}

				Service.addRecieptToStorage(reciept);
				Service.addRecieptToCustomer(reciept, customer);

				//clearAllFields();
			}
		

		}
		
		public void printReciept() {
			/*PdfCreator pdf = new PdfCreator(reciept);*/
			
		}

		public void addPayment() {
			PaymentController.initWindow(reciept);	
		}
		
		
		public void updateInformation() {
			tblRecieptProducts.getItems().clear();
			tblRecieptProducts.getItems().addAll(Service.getRecieptLines(reciept));
			checkCustomerNumber();
			if (tblRecieptProducts.getItems().size() > 0) {
				lblBeforeTax.setText(String.format("%.2f", Service.getTotalRecieptPriceWithoutTax(reciept)));
				lblTax.setText(String.format("%.2f", Service.getTotalRecieptTax(reciept)));
				lblTotal.setText(String.format("%.2f", Service.getTotalRecieptPriceWithTax(reciept)));
				lblAvance.setText("" + String.format("%.2f", Service.getTotalAvanceOnReciept(reciept)) + " %");
			} else {
				tblRecieptProducts.getItems().clear();
				lblBeforeTax.setText("0");
				lblTax.setText("0");
				lblTotal.setText("0");
				lblAvance.setText("");
			}
//			clearAllProductInfo();
		}

		public void clearAllProductInfo() {
			txfEAN.setText("");
			imgProduct.setImage(null);
			lblDescription.setText("");
			btnAdd.setVisible(false);
		}

		public void showProductInformation(Product foundProduct) {
			if (foundProduct != null) {
				lblDescription.setText(foundProduct.desciptionToString());
				lblMessage.setText("");
				tempProduct = foundProduct;
				btnAdd.setVisible(true);

				if (foundProduct.getStockAmount() > 0) {
					btnAdd.setDisable(false);
				} else {
					btnAdd.setDisable(true);
				}

			} else {
				lblMessage.setText("Intet produkt med \nEAN-nummeret: " + txfEAN.getText());
				lblDescription.setText("");
				btnAdd.setVisible(false);
			}

			// Image
			try {
				imgProduct.setImage(Service.findProductImage(foundProduct));
			} catch (FileNotFoundException e) {
				imgProduct.setImage(
						new Image("file:" + System.getProperty("user.dir") + "/src/images/productImages/no_image.png"));
			}
		}
		
		public void addCustomerToReciept(Customer customer) {
			actuelCustomer = customer;
			
			if (customer != null) {
				getCustomerInfo();
				btnNewCustomer.setDisable(false);
			}	
		}
		
		private void checkCustomerNumber() {
			
			if (Service.getRecieptLines(reciept).size() > 0 && !txfCustomerNumber.getText().isEmpty()) {
				btnPayment.setDisable(false);
			}
			else {
				btnPayment.setDisable(true);
			}
		}
		
		 //-------------------RETURN PANE---------------------------//
	    
	    
		public void findReciept() {
			btnReturn.setDisable(true);
			actualReciept = Service.getRecieptByNumber(Integer.parseInt(txfRecieptNumber.getText()));

			if (actualReciept != null) {
				updateReciept();

			}

		}

		public void selectRecieptLine() {
			if (tvRecieptLines.getSelectionModel().getSelectedItem() != null) {

				btnReturn.setDisable(false);
			}

		}

		public void deleteRecieptLine() {
			if (tvRecieptLines.getSelectionModel().getSelectedItem() != null) {

				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Returner vare");
				alert.setHeaderText("Du er ved at returnere en eller flere vare");
				alert.setContentText("Er du sikker på at du vil trække den/de valgte vare retur?");

				Optional<ButtonType> acceptet = alert.showAndWait();

				if (acceptet.get() == ButtonType.OK) {

					RecieptLine actualLine = tvRecieptLines.getSelectionModel().getSelectedItem();

					double refund = 0;

					if (actualLine.getAmount() > 1) {

						ArrayList<Integer> amount = new ArrayList<>();

						for (int i = 1; i < actualLine.getAmount(); i++) {
							amount.add(i);
						}

						ChoiceDialog<Integer> cd = new ChoiceDialog<>(actualLine.getAmount(), amount);
						cd.setHeaderText("Vælg antal");

						Optional<Integer> result = cd.showAndWait();

						if (result.isPresent()) {

							double newSinglePrice = actualLine.getPrice().getPrice() / actualLine.getAmount();

							int newAmount = (actualLine.getAmount() - result.get());

							refund += (result.get() * newSinglePrice);

							Service.updateReturnProduct(actualLine, newSinglePrice * newAmount, newAmount);
						}

					} else {
						refund += actualLine.getPrice().getPrice();

						Service.removeRecieptLine(actualReciept, actualLine);
					}

					updateReciept();

					txfRefund.setText(String.format("%.2f", Double.parseDouble(txfRefund.getText()) + refund));

					if (actualReciept.getRecieptLines().size() == 0) {
						Service.removeReciept(actualReciept);
						tvRecieptLines.setDisable(true);
					}
					btnReturn.setDisable(false);
				}

			}

		}

		public void updateReciept() {
						
			tvRecieptLines.getItems().clear();
			tvRecieptLines.getItems().addAll(Service.getRecieptLinesObserve(actualReciept));
	
		}
	    
	    
	    
	    
	    
	    
	    
}
