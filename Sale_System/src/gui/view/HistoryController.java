package gui.view;

import java.io.IOException;
import java.time.LocalDate;

import application.model.Employee;
import application.model.EmployeeChart;
import application.model.Reciept;
import application.model.RecieptLine;
import application.service.Service;
import gui.MainApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import storage.Storage;

public class HistoryController {
	
	private static TabPane pane;
	private static Stage stage;
	
	@FXML
    private TableView<Reciept> tblSales;

    @FXML
    private TableColumn<Reciept, Integer> columnSaleRecieptNumber;

    @FXML
    private TableColumn<Reciept, Integer> columnSaleCustomerNumber;

    @FXML
    private TableColumn<Reciept, LocalDate> columnSaleRecieptDate;

    @FXML
    private TableColumn<Reciept, Double> columnSaleTotal;

    @FXML
    private DatePicker saleFromDate;

    @FXML
    private DatePicker saleToDate;

    @FXML
    private TableView<Employee> tblEmployeeSales;

    @FXML
    private TableColumn<Employee, String> columnEmployeeName;

    @FXML
    private TableColumn<Employee, Integer> columnEmployeeNumber;

    @FXML
    private TableColumn<Employee, Double> columnEmployeeTotal;

    @FXML
    private TableColumn<Employee, Double> columnEmployeeInsuranceTotal;

    @FXML
    private TableColumn<Employee, Integer> columnEmployeeInsuranceAmount;

    @FXML
    private TableColumn<Employee,Double> columnEmployeeDB;

    @FXML
    private DatePicker employeeFromDate;

    @FXML
    private DatePicker employeeToDate;

    @FXML
    private PieChart pieEmployeeSales;
	
	
	
	 public static void initWindow() {
		 try {			 
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/HistoryView.fxml"));
				pane = (TabPane) loader.load();
				stage = new Stage();
				stage.setTitle("Historik");
				stage.setScene(new Scene(pane));
				stage.show();		
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	 
	@FXML
	void initialize() {
		
		columnSaleRecieptNumber.setCellValueFactory(new PropertyValueFactory<>("recieptNumber"));
		columnSaleCustomerNumber.setCellValueFactory(new PropertyValueFactory<>("CustomerNumber"));
		columnSaleRecieptDate.setCellValueFactory(new PropertyValueFactory<>("date"));
		columnSaleTotal.setCellValueFactory(new PropertyValueFactory<>("totalPrice"));
		
		columnEmployeeName.setCellValueFactory(new PropertyValueFactory<>("name"));
		columnEmployeeNumber.setCellValueFactory(new PropertyValueFactory<>("salesNumber"));
		columnEmployeeTotal.setCellValueFactory(new PropertyValueFactory<>("SumOfAllSoldProductsInclInsurance"));
		columnEmployeeInsuranceTotal.setCellValueFactory(new PropertyValueFactory<>("InsuranceTotal"));
		columnEmployeeInsuranceAmount.setCellValueFactory(new PropertyValueFactory<>("TotalAmountOfInsurances"));
		columnEmployeeDB.setCellValueFactory(new PropertyValueFactory<>("TotalAvance"));
		
		ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
		
		for (EmployeeChart employee : Service.getTotalSaleWithEmployeeData()) {
			pieChartData.add(new PieChart.Data(employee.getName() + " " + String.format("%.2f", employee.getPercent()) + "%" , employee.getPercent()));			
		}
		
		pieEmployeeSales.setData(pieChartData);
		
		tblSales.getItems().setAll(Storage.getAllReciepts());
		tblEmployeeSales.getItems().addAll(Storage.getAllEmployees());
		
		// Listener
		saleFromDate.valueProperty().addListener(event -> updateReciptTable());
		saleToDate.valueProperty().addListener(event -> updateReciptTable());
//		tblSales.getSelectionModel().selectedItemProperty().addListener(event -> openRecieptInformation(pane));
		
		employeeFromDate.valueProperty().addListener(event -> updateNumbers());
		employeeToDate.valueProperty().addListener(event -> updateNumbers());
		
	}
	//Employee table
	public void updateNumbers() {

		tblEmployeeSales.getItems().clear();

		LocalDate from = LocalDate.of(1900, 01, 1);
		LocalDate to = LocalDate.now();

		if (employeeFromDate.getValue() == null || employeeToDate.getValue() == null) {
			if (employeeFromDate.getValue() != null) {
				from = employeeFromDate.getValue();
			}
			if (employeeToDate.getValue() != null) {
				to = employeeToDate.getValue();
			}
		} 
	}
	//Sales table
	public void updateReciptTable() {

		tblSales.getItems().clear();

		LocalDate from = LocalDate.of(1900, 01, 1);
		LocalDate to = LocalDate.now();

		if (saleFromDate.getValue() == null || saleToDate.getValue() == null) {
			if (saleFromDate.getValue() != null) {
				from = saleFromDate.getValue();
			}
			if (saleToDate.getValue() != null) {
				to = saleToDate.getValue();
			}

			tblSales.getItems().addAll(Service.getRecieptsFromDates(from, to));
		} else {
			tblSales.getItems().addAll(Service.getRecieptsFromDates(saleFromDate.getValue(), saleToDate.getValue()));
		}

	}
	
	
	 
}
