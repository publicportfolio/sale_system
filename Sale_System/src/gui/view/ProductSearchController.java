package gui.view;

import java.io.IOException;

import application.model.Product;
import application.service.Service;
import gui.MainApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ProductSearchController {

	
	private static Pane pane;
	private static Stage stage;
	private static SaleReturnController parent;
	private static ObservableList<Product> products = FXCollections.observableArrayList();
	
	@FXML
    private TableView<Product> tblProducts;

    @FXML
    private TableColumn<Product, String> columnBrand;

    @FXML
    private TableColumn<Product, String> columnModel;

    @FXML
    private TableColumn<Product, Double> columnPrice;
    
    @FXML
    private TableColumn<Product, Integer> columnStock;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnOk;

	
	 public static void initWindow(ObservableList<Product> inputProducts,SaleReturnController inputParent) {
	    	try {
	    		
	    		products = inputProducts;
	    		parent = inputParent;
	    		
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/ProductSearchView.fxml"));
				pane = (Pane) loader.load();
				stage = new Stage();
				stage.setTitle("Produkt søgning");
				stage.setScene(new Scene(pane));
				stage.show();
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
	    }
	 
	 @FXML
	 void initialize() {
		
		 columnBrand.setCellValueFactory(new PropertyValueFactory<>("brand"));
		 columnModel.setCellValueFactory(new PropertyValueFactory<>("model"));
		 columnStock.setCellValueFactory(new PropertyValueFactory<>("stockAmount"));
		 columnPrice.setCellValueFactory(new PropertyValueFactory<>("SuggestedRetailPrice"));
		 
		 tblProducts.getItems().setAll(products);
		 
		 
		// Button action
		btnOk.setOnAction(event -> returnWithProduct());
		btnCancel.setOnAction(event -> closeWindow()); 
	 }

	 @FXML
	private void returnWithProduct() {

		Product marked = tblProducts.getSelectionModel().getSelectedItem();
		 
		if (marked != null) {
			parent.showProductInformation(marked);
			stage.close();
		}
	}
	 
	 @FXML
	 private void closeWindow() {
		 stage.close();
	 }
}
