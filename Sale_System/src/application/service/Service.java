package application.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import application.model.Brand;
import application.model.Customer;
import application.model.Employee;
import application.model.EmployeeChart;
import application.model.EnergyMark;
import application.model.Payment;
import application.model.PaymentMethod;
import application.model.Price;
import application.model.Product;
import application.model.ProductType;
import application.model.Reciept;
import application.model.RecieptLine;
import application.model.Smartphone;
import application.model.TVResolution;
import application.model.Tv;
import application.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import storage.Storage;

public class Service {

	// Brands
	public static ArrayList<Brand> getBrands() {
		return Storage.getAllBrands();
	}

	// Insurance
	public static double findInsurancePrice(Price price, int amount, Product product) {

		double returnPrice = -1;
		double pricePrProduct = price.getPrice() / amount;

		if (product instanceof Tv) {

			if (pricePrProduct <= 1999) {
				returnPrice = 599;
			} else if (pricePrProduct >= 2000 && pricePrProduct <= 3999) {
				returnPrice = 799;
			} else if (pricePrProduct >= 4000 && pricePrProduct <= 7999) {
				returnPrice = 799;
			} else if (pricePrProduct >= 8000 && pricePrProduct <= 14999) {
				returnPrice = 1199;
			} else {
				returnPrice = 2199;
			}

		} else {

			if (price.getPrice() <= 1999) {
				returnPrice = 599;
			} else if (pricePrProduct >= 2000 && pricePrProduct <= 3999) {
				returnPrice = 899;
			} else if (pricePrProduct >= 4000 && pricePrProduct <= 7999) {
				returnPrice = 1199;
			} else if (pricePrProduct >= 8000 && pricePrProduct <= 14999) {
				returnPrice = 1499;
			} else {
				returnPrice = 2499;
			}
		}

		return returnPrice;
	}

	public static void addInsuranceToPrice(int amount, Price price, Product product) {

		price.addInsurance(price.getPrice(), amount, product);
	}

	// Reciept line
	public static void createRecieptLine(Reciept reciept, Product product, Price price, int amount) {

		reciept.createRecieptLine(product, price, amount);
		updateStockAmount(product, amount, false);
	}

	public static void addRecieptToCustomer(Reciept reciept, Customer customer) {

		if (reciept != null && customer != null) {
			customer.addReciept(reciept);
			reciept.setCustomer(customer);
			Storage.addCustomer(customer);
			Storage.addReciept(reciept);

			// for (RecieptLine recieptline : reciept.getRecieptLines()) {
			// recieptline.getProduct()
			// .setStockAmount(recieptline.getProduct().getStockAmount() -
			// recieptline.getAmount());
			//
			// }

		}

	}

	/**
	 * Delete an recieptline on a reciept
	 */
	public static void removeRecieptLine(Reciept reciept, RecieptLine recieptLine) {

		if (recieptLine != null) {

			updateStockAmount(recieptLine.getProduct(), recieptLine.getAmount(), true);
			reciept.removeRecieptLine(recieptLine);
		}
	}

	public static void addRecieptLineToReciept(RecieptLine recieptLine, Reciept reciept) {

		reciept.addRecieptLineToReciept(recieptLine);
	}

	// Product
	/**
	 * Find product by EAN-number If a product with that specific ean does not
	 * exist the method returns null.
	 *
	 * @param ean
	 *            : the ean number on the product you what to find.
	 */
	public static Product findProductByEAN(long ean) {

		//DBConnection.getProductByEAN(ean)
		
		return null;
	}

	/**
	 * Find product by search word, returns a list of the objects that contains
	 * the word.
	 *
	 * @param word
	 *            : The serach term the user want to search after
	 */
	public static ObservableList<Product> findProductByText(String searchWord) {

		//DBConnection.getProductsBySearchWord(searchWord)
		
		ArrayList<Product> result = new ArrayList<>();
		
		ObservableList<Product> products = FXCollections.observableArrayList(result);

		for (Product product : Storage.getAllProducts()) {

			if (product.getBrand().getName().toLowerCase().contains(searchWord.toLowerCase())) {
				products.add(product);			} else if (product.getModel().toLowerCase().contains(searchWord.toLowerCase())) {
				products.add(product);
			}
		}
		
		return products;
	}

	/**
	 * Find the avance on actual product.
	 *
	 */
	public static double getAvanceOnProduct(Product product, double price, int amount) {
		double avance = -1;

		double netto = product.getPurchaseNetPrice() * amount;

		double salePrice = ((price) * 0.80);

		avance = ((salePrice - netto) / salePrice) * 100;

		return avance;
	}

	/**
	 * Get the product image for an product, if an image exists
	 */
	public static Image findProductImage(Product product) throws FileNotFoundException {

		Image productImg = null;
		if (product != null) {
			String imgName = (product.getBrand().getName().toLowerCase() + product.getModel().toLowerCase());
			File file = new File(System.getProperty("user.dir") + "/src/images/productImages/" + imgName + ".png");

			if (!file.exists()) {
				file = new File(System.getProperty("user.dir") + "/src/images/productImages/no_image.png");
			}

			productImg = new Image(file.toURI().toString());
		}

		return productImg;
	}

	public static void saveProductImage(File file, String newFilename) {

		File target = new File(System.getProperty("user.dir") + "/src/images/productImages/" + file.getName());

		try {
			Files.copy(file.toPath(), target.toPath().resolveSibling(newFilename), StandardCopyOption.REPLACE_EXISTING);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
	
	// Payment

	public static ArrayList<Payment> getPaymentsForReciept(Reciept reciept){
		return reciept.getPayments();
	}
	
	public static void addPaymentToReciept(Reciept reciept, double amount,PaymentMethod method){
		
		Payment p = new Payment(method, amount, LocalDateTime.now());
		
		reciept.addPayment(p);
				
	}
	
	public static double getBalanceOnReciept(Reciept reciept) {		
		return reciept.getBalanceOnReciept();				
	}
	
	public static void updatePayment(Payment payment, PaymentMethod method, double amount) {		
		payment.setAmount(amount);
		payment.setMethod(method);	
	}
	
	public static void removePayment(Payment payment, Reciept reciept) 
	{
		reciept.removePayment(payment);
	}
	
	
	// Stock
	/**
	 * Get total stock value return stockValue the total value of products in
	 * stock
	 */
	public static double getTotalStockValue() {

		double stockValue = 0;

		for (Product product : Storage.getAllProducts()) {

			if (product.getStockAmount() > 0) {

				stockValue += (product.getStockAmount() * product.getPurchaseNetPrice());
			}

		}

		return stockValue;
	}

	/**
	 * Generates the content for the .csv file
	 */
	public static String generateContentToCsv() {

		double total = 0;
		StringBuilder sb = new StringBuilder();

		sb.append("EAN," + "MÆRKE," + "MODEL,");
		sb.append("ANTAL," + "A PRIS," + "LAGERVÆRDI" + "\n");
		for (Product product : Storage.getAllProducts()) {

			if (product.getStockAmount() > 0) {

				sb.append(product.getEan() + ",");
				sb.append(product.getBrand().getName() + ",");
				sb.append(product.getModel() + ",");
				sb.append(product.getStockAmount() + ",");
				sb.append(product.getPurchaseNetPrice() + ",");
				sb.append(String.format("%.2f", product.getStockAmount() * product.getPurchaseNetPrice()) + "\n");
				total += (product.getStockAmount() * product.getPurchaseNetPrice());
			}
		}
		sb.append(",,,," + "TOTAL:" + "," + String.format("%.2f", total));

		return sb.toString();
	}

	/**
	 * Export products in stock to .csv file
	 */
	public static void createCVSWithStockProducts(File file) {

		try {
			OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
			// FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(generateContentToCsv());
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Update stock value
	 */
	public static void updateStockAmount(Product product, int amount, boolean isReturn) {

		if (product != null) {
			if (isReturn) {
				product.setStockAmount((product.getStockAmount() + amount));
			} else {
				product.setStockAmount((product.getStockAmount() - amount));
			}

		}

	}

	// Employee

	/**
	 * How big part of the total sale each employee has
	 */
	public static ArrayList<EmployeeChart> getTotalSaleWithEmployeeData() {

		ArrayList<EmployeeChart> result = new ArrayList<>();

		double totalSale = 0;

		double partOfTotal;

		for (Reciept reciept : Storage.getAllReciepts()) {
			totalSale += reciept.totalWithInsurance();
		}

		for (Employee employee : Storage.getAllEmployees()) {

			partOfTotal = 0;

			partOfTotal = ((employee.getSumOfAllSoldProductsInclInsurance() / totalSale) * 100);

			EmployeeChart emp = new EmployeeChart(employee.getName(), partOfTotal);

			result.add(emp);

		}

		return result;
	}

	// Customer

	/**
	 * Search for an customer
	 *
	 * @param searchInput
	 *            : The search word that the user has used
	 * @return Arraylist with the customers that were found
	 */
	public static ObservableList<Customer> searchCustomer(String searchInput) {

		String input = searchInput.toLowerCase();

		ObservableList<Customer> resultList = FXCollections.observableArrayList();

		for (Customer customer : Storage.getAllCustomers()) {

			if (customer.getName().toLowerCase().contains(input) || customer.getAddress().toLowerCase().contains(input)
					|| (customer.getPhone() + "").contains(input)) {
				resultList.add(customer);
			}
		}

		return resultList;
	}
	
	

	/**
	 * Avance on reciept
	 */
	public static double getTotalAvanceOnReciept(Reciept reciept) {

		double avance = 0;
		double totalNetto = 0;
		double totalSale = 0;

		for (RecieptLine recieptline : reciept.getRecieptLines()) {

			if (recieptline.getPrice().getInsurance() != null) {
				totalSale += recieptline.getPrice().getInsurance().getPrice();
				totalNetto += (recieptline.getPrice().getInsurance().getPrice() * 0.70);
			}

			totalNetto += recieptline.getAmount() * recieptline.getProduct().getPurchaseNetPrice();
			totalSale += (recieptline.getPrice().getPrice() * 0.80);

		}

		avance = ((totalSale - totalNetto) / totalSale) * 100;

		return avance;
	}

	public static void updateCustomerInformation(Customer customer, String name, String address, int zipcode, int phone,
			String email) {

		if (customer != null) {
			customer.setName(name);
			customer.setAddress(address);
			customer.setZipcode(zipcode);
			customer.setPhone(phone);
			customer.setEmail(email);
		}

	}

	// Reciept

	/**
	 * Returns the reciepts for the dates between from and to date, the two
	 * dates included
	 *
	 * @param from
	 *            : The from date
	 * @param to
	 *            : The to date
	 * @return Returns an arraylist af reciepts
	 */
	public static ArrayList<Reciept> getRecieptsFromDates(LocalDate from, LocalDate to) {

		ArrayList<Reciept> reciepts = new ArrayList<>();

		for (Reciept reciept : Storage.getAllReciepts()) {

			if (reciept.getDate().isAfter(from) || reciept.getDate().isEqual(from)) {
				if (reciept.getDate().isBefore(to) || reciept.getDate().isEqual(to)) {
					if (!reciepts.contains(reciept)) {
						reciepts.add(reciept);
					}
				}

			}
		}

		return reciepts;
	}

	/**
	 * Get total price of the reciept without tax
	 * @return totalPrice
	 */
	public static double getTotalRecieptPriceWithoutTax(Reciept reciept) {

		double total = 0;
		for (RecieptLine line : reciept.getRecieptLines()) {
			total += line.getPrice().getTotalPriceWithInsurance();
		}
			
		return (total*0.80);
	}
		
	/**
	 * Get total price of the reciept with tax
	 * @return totalPrice
	 */
	public static double getTotalRecieptPriceWithTax(Reciept reciept) {
		return (getTotalRecieptPriceWithoutTax(reciept)*1.25);
	}
	
	/**
	 * Get total tax of the reciept
	 * @return totalPrice
	 */
	public static double getTotalRecieptTax(Reciept reciept) {
		return (getTotalRecieptPriceWithTax(reciept)*0.20);
	}

	/**
	 * Find a reciept, with the given reciept number
	 */
	public static Reciept getRecieptByNumber(int recieptNumber) {

		Reciept result = null;

		int i = 0;
		boolean found = false;

		while (i < Storage.getAllReciepts().size() && !found) {

			if (Storage.getAllReciepts().get(i).getNumber() == recieptNumber) {
				result = Storage.getAllReciepts().get(i);
				found = true;
			} else {
				i++;
			}
		}

		return result;

	}

	/**
	 * Remove reciept
	 */
	public static void removeReciept(Reciept reciept) {

		Storage.removeReciept(reciept);

	}

	// Service maintance methods

	public static Reciept createReciept(LocalDate date, Employee employee) {

		Reciept reciept = new Reciept(date, employee);

		return reciept;
	}

	public static void addRecieptToStorage(Reciept reciept) {
		Storage.addReciept(reciept);
		reciept.getEmployee().addReciepttoEmployee(reciept);
	}

	public static Employee createEmployee(String name, LocalDate contractDate, int salesNumber) {

		Employee employee = new Employee(name, contractDate, salesNumber);

		Storage.addEmployee(employee);
		

		return employee;
	}

	public static User createUser(String username, String password, int userLevel, Employee employee) {

		User user = employee.createUser(username, password, userLevel);

		user.setEmployee(employee);

		return user;
	}

	public static Customer createCustomer(String name, String address, int zipcode, int phone, String email) {

		Customer cus = new Customer(name, address, zipcode, phone, email);

		Storage.addCustomer(cus);

		return cus;
	}

	public static Brand createBrand(String name) {

		Brand brand = new Brand(name);

		Storage.addBrand(brand);

		return brand;
	}

	public static Product createProduct(String model, long ean, Brand brand, boolean reserved,
			double suggestedRetailPrice, double purchaseNetPrice, ProductType productType) {

		Product product = new Product(model, ean, brand, reserved, suggestedRetailPrice, purchaseNetPrice,productType);

		Storage.addProduct(product);

		return product;
	}
	

	public static void addProductToStock(Product product, double nettoPrice, int amount) {

		double newNettoPrice = (((product.getStockAmount() * product.getPurchaseNetPrice()) + (amount * nettoPrice))
				/ (amount + product.getStockAmount()));

		product.setStockAmount(product.getStockAmount() + amount);
		product.setPurchaseNetPrice(newNettoPrice);

	}

	public static Tv createTv(String model, long ean, Brand brand, boolean reserved, double suggestedRetailPrice,
			double purchaseNetPrice,ProductType productType, int size, String type, TVResolution resolution, int hdmi, int usb, boolean wifi,boolean bluetooth, boolean smartTv) {

		Tv tv = new Tv(model, ean, brand, reserved, suggestedRetailPrice, purchaseNetPrice,productType, size, type, resolution,
				hdmi, usb, wifi,bluetooth,smartTv);

		Storage.addProduct(tv);

		return tv;

	}

	public static Smartphone createSmartphone(String model, long ean, Brand brand, boolean reserved,
			double suggestedRetailPrice, double purchaseNetPrice, ProductType productType,double screenSize, String cpu, int gb,
			int internalMemory, double weight, double cameraMP, String chargeType) {

		Smartphone smartphone = new Smartphone(model, ean, brand, reserved, suggestedRetailPrice, purchaseNetPrice,productType,
				screenSize, cpu, gb, internalMemory, weight, cameraMP, chargeType);

		Storage.addProduct(smartphone);

		return smartphone;
	}

	public static void updateRecieptLine(RecieptLine productLine, Price price, int amount) {

		productLine.setAmount(amount);
		productLine.setPrice(price);

	}

	public static void updateReturnProduct(RecieptLine productLine, double price, int amount) {

		productLine.setAmount(amount);
		productLine.getPrice().setPrice(price);

		if (productLine.getAmount() == 0) {
			removeRecieptLine(productLine.getReciept(), productLine);
		}

	}

	public static ArrayList<RecieptLine> getRecieptLines(Reciept reciept) {

		return reciept.getRecieptLines();

	}

	public static ObservableList<RecieptLine> getRecieptLinesObserve(Reciept reciept) {
		// Casting arraylist to observableArrayList
		return FXCollections.observableArrayList(reciept.getRecieptLines());

	}

	public static EnergyMark createEnergyMark(String mark) {

		EnergyMark engergymark = new EnergyMark(mark);

		Storage.addEnergyMark(engergymark);

		return engergymark;
	}

	// Init content
	public static void initStorage() {

		// Employee
		Employee emp1 = createEmployee("Mogens", LocalDate.of(2015, 02, 20), 22);
		Employee emp2 = createEmployee("Egon", LocalDate.of(2014, 01, 20), 23);
		Employee emp3 = createEmployee("Johnny Madsen", LocalDate.of(2014, 01, 20), 24);

		// User
		User u1 = createUser("test", "test", 2, emp1);
		User u2 = createUser("egon11", "dsds", 1, emp2);
		User u3 = createUser("egon112", "dsds", 1, emp3);

		// Customer
		Customer c1 = createCustomer("Hans", "Viborgvej 33", 8000, 64645454, "dsasd@gmail.com");
		Customer c2 = createCustomer("Niels", "Viborgvej 33", 8000, 64645454, "dsdssd@gmail.com");
		Customer c3 = createCustomer("Torben", "Viborgvej 33", 8000, 64645454, "dffdfd@gmail.com");
		Customer c4 = createCustomer("Jens", "Viborgvej 33", 8000, 64645454, "pederdingo@gmail.com");

		// Brand
		Brand b1 = createBrand("Sony");
		Brand b2 = createBrand("Samsung");
		Brand b3 = createBrand("LG");
		Brand b4 = createBrand("Philips");

		
		// Product
		Smartphone sp1 = createSmartphone("Galaxy S8", 321321, b2, false, 5888, 3000,ProductType.Smartphone, 5.5, "Snapdragon 4.4Gz quad-core",
				4, 64, 657, 8, "USB-C");
		Smartphone sp2 = createSmartphone("Galaxy S6 Edge", 424242, b2, false, 2323, 1500,ProductType.Smartphone, 5,
				"Snapdragon 3.4Gz quad-core", 3, 32, 879, 5, "Mikro usb");


		Tv tv1 = createTv("KDL32W4000", 4334, b1, false, 3999.95, 1500,ProductType.TV, 32, "LCD", TVResolution.Full_HD, 4, 0, false,false,true);
		Tv tv2 = createTv("UE55H7505", 434345, b2, false, 10099.95, 2400,ProductType.TV, 55, "LED",  TVResolution.Ultra_HD, 5, 3, true,true,true);
		Tv tv3 = createTv("KDL40W4500", 123, b1, false, 4999.95, 3000,ProductType.TV, 40, "LCD",  TVResolution.HD_Ready, 2, 0, false,false,false);

		EnergyMark e1 = createEnergyMark("A+++");
		EnergyMark e2 = createEnergyMark("A++");
		EnergyMark e3 = createEnergyMark("A+");
		EnergyMark e4 = createEnergyMark("A");

		tv1.setEnergyMark(e2);
		tv2.setEnergyMark(e1);
		tv3.setEnergyMark(e4);

		// Stock
		addProductToStock(tv1, 1500, 10);
		addProductToStock(tv2, 2400, 5);
		addProductToStock(tv3, 3000, 12);
		addProductToStock(sp1, 3000, 5);
		addProductToStock(sp2, 1500, 12);

		// Reciept
		Reciept rc1 = createReciept(LocalDate.of(2015, 05, 20), emp2);
		Reciept rc2 = createReciept(LocalDate.of(2015, 02, 23), emp3);

		// This is not in the method, it's first saved when the reciept is
		// confirmed by the user.
		addRecieptToStorage(rc1);
		addRecieptToStorage(rc2);

		// Price
		Price price1 = new Price(4444);
		Price price2 = new Price(5555);
		Price price3 = new Price(4444);
		Price price4 = new Price(5555);

		// RecieptLine
		rc1.createRecieptLine(tv1, price1, 1);
		rc1.createRecieptLine(tv2, price2, 1);

		rc2.createRecieptLine(tv3, price3, 1);
		rc2.createRecieptLine(sp1, price4, 1);

		addRecieptToCustomer(rc1, c1);
		addRecieptToCustomer(rc2, c2);

	}

}
