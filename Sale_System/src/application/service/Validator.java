package application.service;

import java.util.regex.Pattern;

public class Validator {

	private String s;
	
	public Validator() {

	}
	
	
	public boolean isValidString(String s) {
		
		if(s.isEmpty()) {
			return false;
		}
		return true;
	}
	
	public boolean isValidInteger(String s) {
		
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public boolean isValidEmail(String s) {
		
		Pattern regex = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
		return regex.matcher(s).matches();

	}
	

	public boolean isValidPhone(String s) {
		
		Pattern regex = Pattern.compile("([0-9]{2}){3}[0-9]{2}");
		System.out.println(regex.matcher(s).matches());
		return regex.matcher(s).matches();

	}
	
	public boolean isValidZipcode(String s) {
		
		Pattern regex = Pattern.compile("^(?:[1-24-9]\\d{3}|3[0-8]\\d{2})$");
		return regex.matcher(s).matches();

	}
	
}
