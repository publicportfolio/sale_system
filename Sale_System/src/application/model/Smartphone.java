package application.model;

public class Smartphone extends Product {

	private double screenSize;
	private String cpu;
	private int gb;
	private int internalMemory;
	private double weight;
	private double cameraMP;
	private String chargeType;

	public Smartphone(String model, long ean, Brand brand, boolean reserved, double suggestedRetailPrice,
			double purchaseNetPrice,ProductType productType, double screenSize, String cpu, int gb, int internalMemory, double weight,
			double cameraMP, String chargeType) {
	super(model, ean, brand, reserved, suggestedRetailPrice, purchaseNetPrice,productType);
		this.screenSize = screenSize;
		this.cpu = cpu;
		this.gb = gb;
		this.internalMemory = internalMemory;
		this.weight = weight;
		this.cameraMP = cameraMP;
		this.chargeType = chargeType;
	}

	public double getScreenSize() {
		return screenSize;
	}

	public void setScreenSize(double screenSize) {
		this.screenSize = screenSize;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public int getGb() {
		return gb;
	}

	public void setGb(int gb) {
		this.gb = gb;
	}

	public int getInternalMemory() {
		return internalMemory;
	}

	public void setInternalMemory(int internalMemory) {
		this.internalMemory = internalMemory;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getCameraMP() {
		return cameraMP;
	}

	public void setCameraMP(double cameraMP) {
		this.cameraMP = cameraMP;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	@Override
	public String desciptionToString() {
		String text = "";

		StringBuilder sb = new StringBuilder();

		text = super.desciptionToString();

		sb.append("\n\nUdvidet specs:" + "\n");
		sb.append("Skærm: " + this.screenSize + "\"\n");
		sb.append("CPU: " + this.cpu + "\n");
		sb.append("RAM: " + this.gb + "GB \n");
		sb.append("Hukommelse: " + this.internalMemory + " GB\n");
		sb.append("Kamera: " + this.cameraMP + " Megapixel \n");
		sb.append("Oplader: " + this.chargeType + "\n");
		sb.append("Vægt " + this.weight + " g");

		return text + sb.toString();
	}

}
