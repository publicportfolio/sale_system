package application.model;

public class Insurance {

	private double price;
	private int amount;
	private Product product;
	private final int profit = 30;

	public Insurance(double price, int amount, Product product) {
		this.price = price;
		this.amount = amount;
		this.product = product;

		double pricePrProduct = price / amount;

		double insuranceRange = -1;

		if (product instanceof Tv) {

			if (pricePrProduct <= 1999) {
				insuranceRange = 599;
			} else if (pricePrProduct >= 2000 && pricePrProduct <= 3999) {
				insuranceRange = 799;
			} else if (pricePrProduct >= 4000 && pricePrProduct <= 7999) {
				insuranceRange = 799;
			} else if (pricePrProduct >= 8000 && pricePrProduct <= 14999) {
				insuranceRange = 1199;
			} else {
				insuranceRange = 2199;
			}

		} else {

			if (pricePrProduct <= 1999) {
				insuranceRange = 599;
			} else if (pricePrProduct >= 2000 && pricePrProduct <= 3999) {
				insuranceRange = 899;
			} else if (pricePrProduct >= 4000 && pricePrProduct <= 7999) {
				insuranceRange = 1199;
			} else if (pricePrProduct >= 8000 && pricePrProduct <= 14999) {
				insuranceRange = 1499;
			} else {
				insuranceRange = 2499;
			}
		}

		this.price = insuranceRange * amount;
	}

	public int getProfit() {
		return profit;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
