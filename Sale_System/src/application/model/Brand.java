package application.model;

public class Brand {
	// Brand class git test
	//Brand test 2
	private String name;
	private String logo;

	public Brand(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
