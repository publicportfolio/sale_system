package application.model;

public class Price {

	private double price;
	private Insurance insurance;

	public Price(double price) {
		this.price = price;
	}

	// Insurance
	/**
	 * @param price
	 *            : the price on the product
	 * @return returns an object of insurance to the price
	 */
	public void addInsurance(double price, int amount, Product product) {

		Insurance insurance = new Insurance(price, amount, product);

		this.insurance = insurance;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	public double getTotalPriceWithInsurance() {
		double total = 0;

		total = this.price;

		if (this.insurance != null) {
			total += this.insurance.getPrice();
		}

		return total;
	}

	@Override
	public String toString() {
		return String.format("%.2f", this.price);
	}

}
