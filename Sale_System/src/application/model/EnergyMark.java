package application.model;

public class EnergyMark {

	private String mark;

	public EnergyMark(String mark) {
		this.mark = mark;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	@Override
	public String toString() {
		return this.mark;
	}

}
