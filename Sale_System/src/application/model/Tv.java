package application.model;

public class Tv extends Product {

	private int size;
	private String type;
	private TVResolution resolution;
	private int hdmi;
	private int usb;
	private boolean wifi,bluetooth,smartTv;

	public Tv(String model, long ean, Brand brand, boolean reserved, double suggestedRetailPrice,
			double purchaseNetPrice,ProductType productType, int size, String type, TVResolution resolution, int hdmi, int usb, boolean wifi,boolean bluetooth, boolean smartTv) {
		super(model, ean, brand, reserved, suggestedRetailPrice, purchaseNetPrice,productType);
		this.size = size;
		this.type = type;
		this.resolution = resolution;
		this.hdmi = hdmi;
		this.usb = usb;
		this.wifi = wifi;
		this.bluetooth = bluetooth;
		this.smartTv = smartTv;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public TVResolution getResulution() {
		return resolution;
	}

	public void setResulution(TVResolution resolution) {
		this.resolution = resolution;
	}

	public int getHdmi() {
		return hdmi;
	}

	public void setHdmi(int hdmi) {
		this.hdmi = hdmi;
	}

	public int getUsb() {
		return usb;
	}

	public void setUsb(int usb) {
		this.usb = usb;
	}

	public boolean isWifi() {
		return wifi;
	}

	public void setWifi(boolean wifi) {
		this.wifi = wifi;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public String desciptionToString() {
		String text = "";

		StringBuilder sb = new StringBuilder();

		text = super.desciptionToString();

		sb.append("\n\nUdvidet specs:" + "\n");
		sb.append("Størrelse: " + this.size + "\"" + "\n");
		sb.append("Type: " + this.type + "\n");
		sb.append("Opløsning: "+ this.resolution +"\n");
		sb.append("HDMI indgange: " + this.hdmi + "\n");
		sb.append("USB indgange: " + this.usb + "\n");
		sb.append("WIFI: " + (this.wifi ? "Ja" : "Nej") + "\n");
		sb.append("Bluetooth: " + (this.bluetooth ? "Ja" : "Nej") + "\n");
		sb.append("Smart-TV: " + (this.smartTv ? "Ja" : "Nej") + "\n");
		return text + sb.toString();
	}

}
