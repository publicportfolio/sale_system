package application.model;

import java.time.LocalDateTime;

public class Payment {


	private PaymentMethod paymentMethod;
	private double amount;
	private LocalDateTime timestamp;
	

	public Payment (PaymentMethod paymentMethod,double amount,LocalDateTime timestamp) {
		this.paymentMethod = paymentMethod;
		this.amount = amount;
		this.timestamp = timestamp;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public double getAmount() {
		return this.amount;
	}
	
	public void setMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
		}

}
