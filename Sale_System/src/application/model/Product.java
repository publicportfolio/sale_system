package application.model;

public class Product implements Comparable<Product> {

	private String model;
	private long ean;
	private Brand brand;
	private boolean reserved;
	private double suggestedRetailPrice;
	private double purchaseNetPrice;
	private EnergyMark energyMark;
	private int stockAmount;
	private ProductType productType;
	
	public Product(String model, long ean, Brand brand, boolean reserved, double suggestedRetailPrice,
			double purchaseNetPrice,ProductType productType) {
		this.model = model;
		this.ean = ean;
		this.brand = brand;
		this.reserved = reserved;
		this.suggestedRetailPrice = suggestedRetailPrice;
		this.purchaseNetPrice = purchaseNetPrice;
		this.stockAmount = 0;
		this.productType = productType;
	}

	public int getStockAmount() {
		return stockAmount;
	}

	public ProductType getProductCategory() {
		return productType;
	}

	public void setProductCategory(ProductType productType) {
		this.productType = productType;
	}

	public void setStockAmount(int stockAmount) {
		this.stockAmount = stockAmount;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public long getEan() {
		return ean;
	}

	public void setEan(long ean) {
		this.ean = ean;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public boolean isReserved() {
		return reserved;
	}

	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}

	public double getSuggestedRetailPrice() {
		return suggestedRetailPrice;
	}

	public void setSuggestedRetailPrice(double suggestedRetailPrice) {
		this.suggestedRetailPrice = suggestedRetailPrice;
	}

	public double getPurchaseNetPrice() {
		return purchaseNetPrice;
	}

	public void setPurchaseNetPrice(double purchaseNetPrice) {
		this.purchaseNetPrice = purchaseNetPrice;
	}

	public EnergyMark getEnergyMark() {
		return energyMark;
	}

	public void setEnergyMark(EnergyMark energyMark) {
		this.energyMark = energyMark;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("Mærke: " + this.brand);
		sb.append(" Model: " + this.model + "\n");
		sb.append("Antal: " + this.stockAmount);
		sb.append(" Nettopris: " + String.format("%.2f", this.purchaseNetPrice));
		return sb.toString();
	}

	public String desciptionToString() {

		StringBuilder sb = new StringBuilder();

		sb.append("Mærke: " + this.brand + "\n");
		sb.append("Model: " + this.model + "\n");
		sb.append("På lager: " + this.stockAmount + "\n");
		sb.append("Pris: " + String.format("%.2f", this.suggestedRetailPrice) + ",-");

		return sb.toString();
	}

	@Override
	public int compareTo(Product o) {

		int diff = this.brand.getName().compareTo(o.getBrand().getName());

		if (diff == 0) {

			diff = this.getModel().compareTo(o.getModel());
		}

		return diff;
	}

}
