package application.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Employee {

	private String name;
	private LocalDate contractDate;
	private int salesNumber;
	private User user;
	private ArrayList<Reciept> reciepts;

	public Employee(String name, LocalDate contractDate, int salesNumber) {
		this.name = name;
		this.contractDate = contractDate;
		this.salesNumber = salesNumber;
		reciepts = new ArrayList<>();
	}

	public User createUser(String username, String password, int userLevel) {

		User user = new User(username, password, userLevel, this);

		this.user = user;

		return user;
	}

	public void addReciepttoEmployee(Reciept reciept) {

		if (!this.reciepts.contains(reciept)) {
			this.reciepts.add(reciept);
		}

	}

	/**
	 * Total price for sold products ex. insurance
	 *
	 * @result the total price of sold products
	 */
	public double getSumOfAllSoldProductsExInsurance() {

		double result = 0;

		for (Reciept reciept : reciepts) {
			result += reciept.totalExInsurance();
		}

		return result;
	}

	/**
	 * Total price for sold products incl. insurance
	 *
	 * @result the total price of sold products
	 */
	public double getSumOfAllSoldProductsInclInsurance() {

		double result = 0;

		for (Reciept reciept : reciepts) {
			result += reciept.totalWithInsurance();
		}

		return result;
	}

	/**
	 * Total amount of sold insurances
	 *
	 * @result the total amount of insurances
	 */
	public int getTotalAmountOfInsurances() {

		int result = 0;

		for (Reciept reciept : reciepts) {
			result += reciept.insuranceCountOnReciept();
		}

		return result;
	}

	/**
	 * Getting total avance from all sales
	 */
	public double getTotalAvance() {

		double totalAvance = 0;

		double salesPrice = 0;
		double nettoPrice = 0;

		for (Reciept reciept : reciepts) {
			for (RecieptLine recieptline : reciept.getRecieptLines()) {

				salesPrice += recieptline.getPrice().getPrice();
				nettoPrice += ((recieptline.getProduct().getPurchaseNetPrice())*recieptline.getAmount());

			}
		}

		double newSalesPrice = ((salesPrice) * 0.80);

		totalAvance = ((newSalesPrice - nettoPrice) / newSalesPrice) * 100;

		totalAvance =  (Math.floor(totalAvance * 100) / 100);
		
		return totalAvance;
	}

	public ArrayList<Reciept> getReciepts() {
		return reciepts;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getContractDate() {
		return contractDate;
	}

	public void setContractDate(LocalDate contractDate) {
		this.contractDate = contractDate;
	}

	public int getSalesNumber() {
		return salesNumber;
	}

	public void setSalesNumber(int salesNumber) {
		this.salesNumber = salesNumber;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		
		sb.append(String.format("%-20s",this.name));
		sb.append(String.format("%-8s", "Nr: " + this.salesNumber));
		sb.append(String.format("%-20s", "Omsætning: " + getSumOfAllSoldProductsInclInsurance()));
		sb.append(String.format("%-24s", "HerafForsikring: " + (getSumOfAllSoldProductsInclInsurance() - getSumOfAllSoldProductsExInsurance())));
		sb.append(String.format("%-10s", "Antal: " + getTotalAmountOfInsurances()));
		sb.append(String.format("%-12s", "Avance: " + String.format("%.2f", getTotalAvance())));

		return sb.toString();
	}

	//----Tableview methods -----------//
	
	public double getInsuranceTotal() {	
		return (getSumOfAllSoldProductsInclInsurance() - getSumOfAllSoldProductsExInsurance());
	}
	
	
	
	
}
