package application.model;

import java.time.LocalDate;
import java.util.ArrayList;

import javafx.collections.ObservableList;

public class Reciept {

	private static int recieptCounter = 1;
	private int recieptNumber;
	private LocalDate date;
	private Customer customer;
	private Employee employee;
	private ArrayList<RecieptLine> recieptLines;
	private ArrayList<Payment> payments;
	
	public Reciept(LocalDate date, Employee employee) {
		this.date = date;
		this.employee = employee;
		this.recieptLines = new ArrayList<RecieptLine>();
		this.recieptCounter++;
		this.recieptNumber = recieptCounter;
		this.payments = new ArrayList<>();
	}

	public int getNumber() {
		return recieptNumber;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public ArrayList<RecieptLine> getRecieptLines() {
		return recieptLines;
	}

	public void addRecieptLineToReciept(RecieptLine recieptLine) {

		if (!this.recieptLines.contains(recieptLine)) {
			this.recieptLines.add(recieptLine);
		}
	}

	public double totalExInsurance() {
		double result = 0;

		for (RecieptLine recieptLine : recieptLines) {
			result += recieptLine.getPrice().getPrice();
		}
		return result;
	}

	public double totalWithInsurance() {
		double result = 0;

		for (RecieptLine recieptLine : recieptLines) {
			result += recieptLine.getPrice().getTotalPriceWithInsurance();
		}
		return result;
	}

	public int insuranceCountOnReciept() {

		int count = 0;

		for (RecieptLine recieptLine : recieptLines) {

			if (recieptLine.getPrice().getInsurance() != null) {
				count += recieptLine.getAmount();
			}
		}

		return count;
	}

	public void removeRecieptLine(RecieptLine recieptLine) {

		if (this.recieptLines.contains(recieptLine)) {

			this.recieptLines.remove(recieptLine);
		}

	}

	public RecieptLine createRecieptLine(Product product, Price price, int amount) {

		RecieptLine rl = new RecieptLine(product, price, this, amount);

		this.recieptLines.add(rl);

		return rl;

	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getRecieptNumber() {
		return recieptNumber;
	}

	public double getTotalPrice() {

		double total = 0;

		for (RecieptLine recieptLine : recieptLines) {

			total += recieptLine.getPrice().getPrice();

			if (recieptLine.getPrice().getInsurance() != null) {
				total += recieptLine.getPrice().getInsurance().getPrice();
			}
		}

		return total;
	}
	
	
	//Payments

	public ArrayList<Payment> getPayments() {
		return payments;
	}
	
	public void addPayment(Payment payment) {
		if (!this.payments.contains(payment)) {
			this.payments.add(payment);
		}
	}
	
	public void removePayment(Payment payment) {
		if (this.payments.contains(payment)) {
			this.payments.remove(payment);
		}
	}
	
	public double getBalanceOnReciept() {
		
		double result = getTotalPrice();
		
		for (Payment payment : payments) {
			result -= payment.getAmount();
		}
		return result;
	}
	

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("Faktura: " + this.recieptNumber);
		sb.append("\tKunde nummer: " + this.customer.getCustomerNumber());
		sb.append("\tDato: " + this.date);
//		sb.append("\tTotal: " + String.format("%.2f", this.totalPrice()) + ",-");

		return sb.toString();

	}

	
	//----Tableview methods -----------//
	
	public Integer getCustomerNumber() {
		return this.customer.getCustomerNumber();
	}
}
