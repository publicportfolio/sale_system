package application.model;

public class User {

	private String username;
	private String password;
	private int userLevel;
	private Employee employee;
	
	public User(String username, String password, int userLevel, Employee employee) {
		this.username = username;
		this.password = password;
		this.userLevel = userLevel;
		this.employee = employee;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	
	
	
}
