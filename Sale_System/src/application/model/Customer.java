package application.model;

import java.util.ArrayList;

public class Customer implements Comparable<Customer> {

	private String name;
	private String address;
	private int phone;
	private int zipcode;
	private String email;
	private int customerNumber = 0;
	private static int customerCounter = 0;
	private ArrayList<Reciept> reciepts;

	public Customer(String name, String address, int zipcode, int phone, String email) {
		this.name = name;
		this.address = address;
		this.zipcode = zipcode;
		this.phone = phone;
		this.email = email;
		this.customerCounter++;
		this.customerNumber = customerCounter;

		this.reciepts = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getZipcode() {
		return zipcode;
	}

	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public ArrayList<Reciept> getReciepts() {
		return reciepts;
	}

	public void setReciepts(ArrayList<Reciept> reciepts) {
		this.reciepts = reciepts;
	}

	public void addReciept(Reciept reciept) {
		if (!reciepts.contains(reciept)) {
			reciepts.add(reciept);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(this.name);
		sb.append(" " + this.address);
		sb.append(" " + this.phone);

		return sb.toString();
	}

	@Override
	public int compareTo(Customer o) {

		int diff = this.name.compareTo(o.getName());

		if (diff == 0) {
			diff = this.address.compareTo(o.address);

			if (diff == 0) {
				diff = this.zipcode - o.zipcode;
			}
		}

		return diff;
	}

}
