package application.model;

public class RecieptLine {

	private Product product;
	private Price price;
	private Reciept reciept;
	private int amount;

	public RecieptLine(Product product, Price price, Reciept reciept, int amount) {
		this.product = product;
		this.price = price;
		this.reciept = reciept;
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Reciept getReciept() {
		return reciept;
	}

	public void setReciept(Reciept reciept) {
		this.reciept = reciept;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("Mærke: " + this.product.getBrand() + "\n");
		sb.append("Model: " + this.product.getModel() + "\n");
		sb.append("Stk.: " + this.amount + "\n");
		sb.append("Pris: " + String.format("%.2f",this.price.getPrice()) + ",- \n");
		if (this.price.getInsurance() != null) {
			sb.append("Forsikring: " + this.getPrice().getInsurance().getPrice() + ",- \n");
		} else {
			sb.append("Forsikring er fravalgt" + "\n");
		}
		return sb.toString();
	}
	
	//Printing methods
	
	public String getProductBrand(){
		return this.product.getBrand().getName();
	}
	public String getProductModel(){
		return this.product.getModel();
	}
	
	public Double getProductInsurance(){
		
		double result = 0;
		
		if (this.getPrice().getInsurance() != null) {
			result = this.getPrice().getInsurance().getPrice();
		}
		
		return result;
	}
	
}
