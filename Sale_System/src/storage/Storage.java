package storage;

import java.util.ArrayList;
import java.util.Collections;
import application.model.Brand;
import application.model.Customer;
import application.model.Employee;
import application.model.EnergyMark;
import application.model.Product;
import application.model.Reciept;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class Storage {

	//ArrayLists
	private static ArrayList<Customer> allCustomers = new ArrayList<>();
	private static ArrayList<Reciept> allReciepts = new ArrayList<>();
	private static ArrayList<Product> allProducts = new ArrayList<>();
	private static ArrayList<Brand> allBrands = new ArrayList<>();
	private static ArrayList<Employee> allEmployees = new ArrayList<>();
	private static ArrayList<EnergyMark> allEnergyMarks = new ArrayList<>();

	//ObservableLists
	private static ObservableList<Customer> allObserveCustomers = FXCollections.observableArrayList(); 
	private static ObservableList<Reciept> allObserveReciepts = FXCollections.observableArrayList();
	private static ObservableList<Product> allObserveProducts = FXCollections.observableArrayList(); 
	private static ObservableList<Employee> allObserveEmployees = FXCollections.observableArrayList(); 
	
	//Employee
	public static ObservableList<Employee> getAllObserveEmployees() {
		return allObserveEmployees;
	}
	
	public static ArrayList<Employee> getAllEmployees() {
		return allEmployees;
	}

	public static void addEmployee(Employee employee) {
		if (!allEmployees.contains(employee)) {
			allEmployees.add(employee);
			allObserveEmployees.add(employee);
		}
	}
	
	// Customer
	public static ArrayList<Customer> getAllCustomers() {
		return allCustomers;
	}
	
	public static ObservableList<Customer> getAllObserveCustomers() {
		return allObserveCustomers;
	}

	public static void addCustomer(Customer customer) {
		if (!allCustomers.contains(customer)) {
			allCustomers.add(customer);
			allObserveCustomers.add(customer);
		}
	}
	

	// Reciept
	public static ArrayList<Reciept> getAllReciepts() {
		return allReciepts;
	}
	
	public static ObservableList<Reciept> getAllObserveReciepts() {
		return allObserveReciepts;
	}

	public static void addReciept(Reciept reciept) {
		if (!allReciepts.contains(reciept)) {
			allReciepts.add(reciept);
			allObserveReciepts.add(reciept);
		}
	}
	public static void removeReciept(Reciept reciept){
		if (allReciepts.contains(reciept)) {
			allReciepts.remove(reciept);
			allObserveReciepts.remove(reciept);
		}
	}

	// Product
	public static ArrayList<Product> getAllProducts() {
		
		Collections.sort(allProducts);
			
		return allProducts;
	}
	
public static ObservableList<Product> getAllObserveProducts() {
		
		Collections.sort(allObserveProducts);
			
		return allObserveProducts;
	}

	public static void addProduct(Product product) {
		if (!allProducts.contains(product)) {
			allProducts.add(product);
			allObserveProducts.add(product);
		}
	}

	// Brand
	public static ArrayList<Brand> getAllBrands() {
		return allBrands;
	}

	public static void addBrand(Brand brand) {
		if (!allBrands.contains(brand)) {
			allBrands.add(brand);
		}
	}

	// EnergyMark
	public static ArrayList<EnergyMark> getAllEnergymarks() {
		return allEnergyMarks;
	}

	public static void addEnergyMark(EnergyMark energymark) {
		if (!allEnergyMarks.contains(energymark)) {
			allEnergyMarks.add(energymark);
		}
	}

}
